require "rails_helper"

RSpec.feature "Users can edit existing projects" do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project, name: "Atom editor") }

  before do
    login_as(user)
    assign_role!(user, :manager, project)

    visit "/"
    click_link "Atom editor"
    click_link "Edit Project"
  end

  scenario "with valid attributes" do
    fill_in "Name", with: "Vim 7.4"
    click_button "Update Project"

    expect(page).to have_content "Project updated!"
    expect(page).to have_content "Vim 7.4"
  end

  
  scenario "with invalid attributes" do
    fill_in "Name", with: ""
    click_button "Update Project"

    expect(page).to have_content "Project not updated!"
  end
end
