require "rails_helper"

RSpec.feature "Users can delete Projects" do
  before do
    login_as(FactoryGirl.create(:user, :admin))
  end

  scenario "successfully" do
    FactoryGirl.create(:project, name: "Atom editor")
    
    visit "/"

    click_link "Atom editor"
    click_link "Delete Project"

    expect(page).to have_content "Project deleted!"
    expect(page.current_url).to eq projects_url
    expect(page).to have_no_content "Atom editor"
  end
end
