require 'rails_helper'

RSpec.feature 'users can create projects' do
  before do
    login_as(FactoryGirl.create(:user, :admin))
    visit '/'

    click_link "New Project"
  end
  
  scenario 'with valid attributes' do
    fill_in 'Name', with: 'Atom editor'
    fill_in 'Description', with: 'A text editor for the future'
    click_button 'Create Project'

    expect(page).to have_content 'Project created!'

    project = Project.find_by(name: "Atom editor")
    expect(page.current_url).to eq project_url(project)

    title = "Atom editor - Projects - Ticketing"
    expect(page).to have_title title
  end

  scenario 'with invalid attributes' do
    click_button 'Create Project'

    expect(page).to have_content "Project not created!"
    expect(page).to have_content "Name can't be blank"
  end
end
