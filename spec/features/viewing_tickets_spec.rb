require "rails_helper"

RSpec.feature "Users can view tickets" do
  before do
    author = FactoryGirl.create(:user)

    ticket1 = FactoryGirl.create(:project, name: "Test Project")
    assign_role!(author, :viewer, ticket1)
    FactoryGirl.create(:ticket, project: ticket1,
                       author: author,
                       name: "Test Ticket",
                       description: "This is a test ticket!")

    ticket2 = FactoryGirl.create(:project, name: "Test Project2")
    assign_role!(author, :viewer, ticket2)
    FactoryGirl.create(:ticket, project: ticket2,
                       author: author,
                       name: "Test Ticket 2",
                       description: "Another test ticket!")

    login_as(author)
    visit "/"
  end

  scenario "for a given project" do
    click_link "Test Project"

    expect(page).to have_content "Test Ticket"
    expect(page).to_not have_content "Test Ticket 2"

    click_link "Test Ticket"
    within("#ticket h2") do
      expect(page).to have_content "Test Ticket"
    end

    expect(page).to have_content "This is a test ticket!"
  end
end
