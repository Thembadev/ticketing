require "rails_helper"

RSpec.feature "Users can create tickets" do
  let(:user) { FactoryGirl.create(:user) }

  before do
    login_as(user)
    project = FactoryGirl.create(:project, name: "Test project")
    assign_role!(user, :viewer, project)

    visit project_path(project)
    click_link "New Ticket"
  end

  scenario "with valid attributes" do
    fill_in "Name", with: "Test Ticket"
    fill_in "Description", with: "This is a test ticket."
    click_button "Create Ticket"

    expect(page).to have_content "Ticket created!"
    within("#ticket") do
      expect(page).to have_content "Author: #{user.email}"
    end
  end

  scenario "with invalid attributes" do
    click_button "Create Ticket"

    expect(page).to have_content "Ticket not created!"
    expect(page).to have_content "Name can't be blank"
    expect(page).to have_content "Description can't be blank"
  end
  
  scenario "with invalid description" do
   fill_in "Name", with: "Test Ticket"
  fill_in "Description", with: "Yes!" 
    click_button "Create Ticket"

    expect(page).to have_content "Ticket not created!"
    expect(page).to have_content "Description is too short"
  end
end
