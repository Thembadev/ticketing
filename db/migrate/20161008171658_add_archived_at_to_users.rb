class AddArchivedAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :archive_at, :timestamp
  end
end
