class ChangeArchiveAtToArchivedAtInUsers < ActiveRecord::Migration
  def change
  	rename_column :users, :archive_at, :archived_at
  end
end
