class TicketsController < ApplicationController
  before_action :set_project
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  def new
    @ticket = @project.tickets.build
  end

  def create
    @ticket = @project.tickets.build(ticket_params)
    @ticket.author = current_user

    if @ticket.save
      flash[:notice] = "Ticket created!"
      redirect_to [@project, @ticket]
    else
      flash[:alert] = "Ticket not created!"
      render "new"
    end
  end

  def show
    authorize @ticket, :show?
  end

  def edit
  end

  def destroy
  end

  def destroy
    @ticket.destroy
    flash[:notice] = "Ticket Deleted."
  
    redirect_to @project
  end

  def update
    if @ticket.update(ticket_params)
      flash[:notice] = "Ticket Updated."
      redirect_to [@project, @ticket]
    else
      flash[:alert] = "Ticket Not Updated."
      render "edit"
    end
  end

  private
  def set_project
    @project = Project.find(params[:project_id])
  end
  
  def set_ticket
    @ticket = @project.tickets.find(params[:id])
  end
  def ticket_params
    params.require(:ticket).permit(:name, :description)
  end
end
